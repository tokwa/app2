<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Task;
use App\Project;

class ProjectTasksController extends Controller
{

    public function store(Project $project) {

        // Task::create([
        //     'project_id' => $project->id,
        //     'description' => request('description')
        // ]);

        $attributes = request()->validate(['description' => 'required']);

        $project->addTask($attributes);
        
        return back();
    }


    public function update(Task $task) {

        // below line is optional ka partner nito ung complete() and incomplete() functions na nasa task model

        // if (request()->has('completed')) {
            
        //     $task->complete();

        // } else {

        //     $task->incomplete();

        // }
        
        // $task->update([
        //     'completed' => request()->has('completed') // completed is table name
        // ]);

        // if ($task->completed == 1) {
        //     return 1;
        // } else {
        //     return 2;
        // }

        $task->complete(request()->has('completed'));

        return back();
    }
}
