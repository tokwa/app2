<?php

namespace App\Http\Controllers;
use App\Project;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Filesystem\Filesystem;

class ProjectsController extends Controller
{

    public function __construct() {

        $this->middleware('auth');

        // $this->middleware('auth')->only(['store', ['update']]);
        // $this->middleware('auth')->except(['store', ['update']]);
    }


    public function index() {

       // auth()->id();  4

       // auth()->user();  user

       // auth()->check();  boolean

        //if (auth()->guest())

        // $projects = Project::where('owner_id', auth()->id())->paginate(5);

        $projects = auth()->user()->projects; // gets all the project for the auth user
        

        return view('projects.index', compact('projects'));
        // $posts = Post::orderBy('created_at', 'desc')->where('user_id', Auth::user()->id)->paginate(5);
        // $projects = Project::orderBy('created_at', 'desc')->paginate(5);
        // return view('projects.index')->with('projects', $projects);
    }

    public function create() {
        
        return view('projects/create');
    }

    public function store() {
        
        // $project = new Project();
        // $project->title = request('title');
        // $project->description = request('description'); 
        // $project->save();

        // request()->validate([
        //     'title' => ['required', 'min:3', 'max:255'],
        //     'description' => ['required', 'min:5', 'max:255']
        // ]);

        // request()->validate([
        //     'title' => ['required'],
        //     'description' => ['required']
        // ]);

        // Project::create(request(['title', 'description']));

        $validated = request()->validate([
            'title' => ['required'],
            'description' => ['required']
        ]);

        Project::create($validated + ['owner_id' => auth()->id()]);

        // $x = new Project;
        // $x->title = request()->title;
        // $x->description = request()->description;
        // // $x->owner_id = Auth::user()->id;
        // $x->save();

        return redirect('/projects');
    }

    public function show(Project $project) {

        // //$project = Project::findOrFail($id);

        // //dd($project);

        if ($project->owner_id == auth()->id() || auth()->id() == 1) {
            
            return view('projects.show', compact('project'));

        } else {

            abort(403);
        }

        // abort_if($project->owner_id !== auth()->id(), 403);

        
    }

    // public function show(Filesystem $file) {

    //     dd($file);
    // }
    
    public function edit(Project $project) {

        // $project = Project::findOrFail($id);

        return view('projects.edit', compact('project'));
    }

    public function update(Project $project) {

        // $project = Project::find($id);

        // $project->title = request('title');
        // $project->description = request('description');
        // $project->save();

        $validated = request()->validate([
            'title' => ['required'],
            'description' => ['required']
        ]);

        $project->update($validated);

        return redirect('/projects');
    }

    public function destroy(Project $project) {
        
        $project->delete();
        return redirect('/projects');
    }

    public function test() {

        return view('projects.test');
    }

    public function test2() {

        return view('projects.test2');
    }

    
}
