@extends('layouts.app')
@section('content')
<div class="container">
    <h1>Create a new project</h1>

    <form method="POST" action="{{route('projects.store')}}">
        {{ csrf_field() }}
        <input type="text" name="title" placeholder="Title" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" value="{{old('title')}}">
        @if ($errors->has('title'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('title') }}</strong>
            </span>
        @endif
        <br>
        <textarea rows="6" name="description" placeholder="Description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}">{{old('description')}}</textarea>
        @if ($errors->has('description'))
            <span class="invalid-feedback" role="alert">
            <strong>{{ $errors->first('description') }}</strong>
            </span>
        @endif
        <br>
        <button type="submit">Create Project</button>
        {{-- @include('errors') --}}
    </form>
</div>
    @endsection
