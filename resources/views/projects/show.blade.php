@extends('layouts.app')

@section('content')

<div class="container">
<h1>Show page</h1>
<div>
    Title: {{$project->title}}
</div>
<div>
    Title: {{$project->description}}
</div>
<a href="/projects/{{$project->id}}/edit">Edit</a>
@if($project->tasks->count()>0)

@foreach($project->tasks as $task)
<form method="POST" action="/tasks/{{$task->id}}">
    @method('PATCH')
    @csrf
    <div class="form-check"></div>
    <input type="checkbox" class="form-check-input" id="exampleCheck1" name="completed" onchange="this.form.submit();"
        {{$task->completed ? 'checked' : ''}}>
    {{$task->description}}
</form>
@endforeach
@endif
{{-- adding a new task --}}
<form method="POST" action="/projects/{{$project->id}}/tasks">
    {{ csrf_field() }}
    <input type="text" name="description" placeholder="Title" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}"
        value="{{old('title')}}">
    @if ($errors->has('description'))
    <span class="invalid-feedback" role="alert">
        <strong>{{ $errors->first('description') }}</strong>
    </span>
    @endif
    <br>
    <button type="submit">Create New Task</button>
    {{-- @include('errors') --}}
</form>
</div>
@endsection

