@extends('layouts.app')

@section('content')
<div class="container-fluid">
<h1>Projects</h1>
<div class="row">
    @if(count($projects) > 0)
    @foreach ($projects as $project)
    <div class="col-sm-4">
        <div class="card">
            <h5 class="card-header">Featured</h5>
            <div class="card-body">
                <h5 class="card-title"><a href="projects/{{$project->id}}">{{$project->title}}</a></h5>
                <p class="card-text">
                    {{str_limit(strip_tags($project->description), 100)}}
                    @if (strlen(strip_tags($project->description)) > 100)
                    <br>
                    <a href="projects/{{$project->id}}">Show more</a>
                    @endif
                </p>
            </div>
        </div>
    </div>
    @endforeach
</div>
{{-- {{$projects->links()}} --}}
@else
<p>There is no post at the moment.</p>
@endif
</div>
@endsection
