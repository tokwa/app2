@extends('layouts.app')

@section('content')
<div class="container">
<form method="POST" action="/projects/{{$project->id}}">
        <div class="form-group">
            {{method_field('PATCH')}}
            {{ csrf_field() }}
            <label for="title">Title</label>
            <input type="text" class="form-control" id="title" name="title" placeholder="Title" value="{{$project->title}}">
        </div>
        <div class="form-group">
            <label for="description">Description</label>
            <br>
        <textarea name="description" id="" cols="155" rows="10" value>{{$project->description}}</textarea>
            @include('errors')
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <form method="POST" action="/projects/{{$project->id}}"> 
        @method('DELETE')
        @csrf

        <button type="submit" class="btn btn-danger">Delete</button>
        </form>
</div>
@endsection
