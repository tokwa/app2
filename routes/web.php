<?php


use Illuminate\Filesystem\Filesystem;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// app()->bind('example', function(){
//     return new \App\Example;
// });

// Route::get('/', function () {

//     dd(app('example'));

//     return view('welcome');
// });



Auth::routes();

// Route::get('/', 'PagesController@index')->name('homee');
// Route::get('/about', 'PagesController@about')->name('about');
// Route::get('/contact', 'PagesController@contact')->name('contact');
Route::get('/home', 'HomeController@index')->name('home');

// Route::get('/home', 'HomeController@index')->name('home')-middleware('guest');


Route::get('/test', 'ProjectsController@test');
Route::get('/', 'ProjectsController@index');
Route::resource('projects', 'ProjectsController');



Route::post('/projects/{project}/tasks', 'ProjectTasksController@store');
Route::patch('/tasks/{task}', 'ProjectTasksController@update');




// Route::post('/projects', 'ProjectsController@store')->name('store');
// Route::get('/projects/create', 'ProjectsController@create');

